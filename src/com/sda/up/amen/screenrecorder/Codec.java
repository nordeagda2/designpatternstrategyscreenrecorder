package com.sda.up.amen.screenrecorder;

public enum Codec {
	h264, h263, theora, vr8
}
