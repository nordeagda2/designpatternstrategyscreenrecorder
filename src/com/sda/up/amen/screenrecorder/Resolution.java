package com.sda.up.amen.screenrecorder;

public enum Resolution {
	R1920x1080, R1366x768,R1280x800
}
